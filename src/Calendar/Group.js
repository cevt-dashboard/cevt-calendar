import React from 'react';
import './css/Group.css';
import {
  CCardGroup,
} from '@coreui/react'
import {
  CFormGroup,
  CCol,
  CSelect
} from '@coreui/react'
import {
  getGroupList,
  getTeacherList,
  getGroupDetails,
  getTeacherDetails,
  getTitle
} from '../_services/groups.service';
import { 
  Inject, 
  ScheduleComponent, 
  Day, 
  Week, 
  WorkWeek, 
  Month, 
  ViewsDirective, 
  ViewDirective 
} from '@syncfusion/ej2-react-schedule';
import { Internationalization, L10n, loadCldr, setCulture} from '@syncfusion/ej2-base';

L10n.load({
  'fr-CH': {
    'schedule': {
      "day": "Jour",
      "week": "Semaine",
      "workWeek": "Semaine de Travail",
      "month": "Mois",
      "agenda": "Agenda",
      "weekAgenda": "Week Agenda",
      "workWeekAgenda": "Work Week Agenda",
      "monthAgenda": "Month Agenda",
      "today": "Aujourd'hui",
    }
  }
});
setCulture('fr-CH');

loadCldr(
  require('cldr-data/supplemental/numberingSystems.json'),
  require('cldr-data/main/fr-CH/ca-gregorian.json'),
  require('cldr-data/main/fr-CH/numbers.json'),
  require('cldr-data/main/fr-CH/timeZoneNames.json'));

class Group extends React.Component {

    constructor(props) {
        super(props);
        this.instance = new Internationalization();
        this.workingDays = [1, 2, 3, 4, 5, 6];
        this.fields =  {
          id: 'Id',
          subject: { name: 'Subject' },
          isAllDay: { name: 'IsAllDay' },
          startTime: { name: 'dtStart' },
          endTime: { name: 'dtEnd' },
          type: {name: 'type'},
          isReadOnly: {name: 'IsReadOnly'},
          location : {name: 'Location'},
          primaryColor : {name: 'PrimaryColor'}
        };
        this.state= {
          liste: [],
          activeElement: null,
          displayView: 'Week',
          placeholder: '',
          data : [{
            Uid: 2,
            Subject: 'Meeting',
            dtStart: new Date(2020, 1, 15, 10, 0),
            dtEnd: new Date(2020, 1, 15, 12, 30),
            teacher: '',
            IsAllDay: false,
            Status: 'Completed',
            Priority: 'High',
            IsReadonly: true,
          }]
        }
      }
      
      componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
        if (this.props.isTeacher){
          getTeacherList().then((data) => {
            console.log('toto')
            this.setState({ 
              liste:  data, 
              placeholder : 'Selectionner un professeur'
            })
            if(localStorage.getItem('selectedCalendarItem')) {
              this.selectTeacher(localStorage.getItem('selectedCalendarItem'))
            }
          });
        }
        else{
          getGroupList().then((data) => {
            this.setState({ 
              liste:  data, 
              placeholder : 'Selectionner une promotion'
            })
            if(localStorage.getItem('selectedCalendarItem')) {
              this.selectGroup(localStorage.getItem('selectedCalendarItem'))
            }
          });
        }
      }
      
      updateDimensions = () => {
        if(window.innerWidth <= 550 ) {
          
          this.setState({ displayView: 'Day' });
        } else {
          this.setState({ displayView: 'Week' });
        }
        
      };

      selectGroup =  (event) => {
        var group = {list: event.target.value.split(' ')[0]};
        getGroupDetails(group.list).then((json) => {

          var datareceived = [];
          json.forEach(value => {

            var startTime = new Date(value.dtStart);
            startTime.setHours(startTime.getHours() + 1);
            var endTime = new Date(value.dtEnd);
            endTime.setHours(endTime.getHours() + 1);
            datareceived.push({
              Id: value.uid,
              Subject: getTitle(value.summary),
              dtStart: startTime,
              dtEnd: endTime,
              teacher: value.description.groupe,
              type: value.description.type,
              IsAllDay: false,
              Location: value.location,
              Priority: '',
              IsReadonly: true
            });
          });

          this.setState({
            data : datareceived,
            activeElement: group
          });
          localStorage.setItem('selectedCalendarItem', event);
        });
      }

      selectTeacher =  (event) => {
        var lastName = {target1: event.target.value.split(' ')[0]};
        var firstName = {target2: event.target.value.split(' ')[1]};
        getTeacherDetails(lastName.target1, firstName.target2).then((json) => {
          var datareceived = [];
          json.forEach(value => {
            var startTime = new Date(value.dtStart);
            startTime.setHours(startTime.getHours() + 1);
            var endTime = new Date(value.dtEnd);
            endTime.setHours(endTime.getHours() + 1);

            datareceived.push({
              Id: value.uid,
              Subject: getTitle(value.summary),
              dtStart: startTime,
              dtEnd: endTime,
              teacher: value.calName,
              type: value.description.type,
              IsAllDay: false,
              Location: value.location,
              Priority: '',
              IsReadonly: true
            });
          });

          this.setState({
            data : datareceived,
            activeElement: lastName+' '+firstName
          });
          localStorage.setItem('selectedCalendarItem', event);
        });
      }
    
      eventTemplateMonth(props){
        return (
          <div className={props.type} style={{width: "212px", textAlign:"center"}}>
            <div className="e-subject" >
              {props.Subject}
            </div>
            <div className="e-location">
              {props.Location}
            </div>
          </div>
        )
      }
     
      splitDate(i) {

        return i < 10 ? i = "0" + i : i;
      }
    
      eventTemplate(props) {
        return (
          <div className={props.type} 
            style={{
              height: "100%", 
              textAlign:"center"}}>
            <div className="header-Calendar">
              <div className="e-subject" >
                {props.type} - 
                {this.splitDate(props.dtStart.getHours())}h
                {this.splitDate(props.dtStart.getMinutes())} - 
                {this.splitDate(props.dtEnd.getHours())}h
                {this.splitDate(props.dtEnd.getMinutes())}
              </div>
            </div>
            <div className="body-Calendar">
              <div className="e-title">
                {props.Subject}
              </div>        
              <div className="e-teacher">
                {props.teacher}
              </div>
              { props.Location.trim() !== "" &&
              <div className="e-location" >
                Salle : {props.Location}
              </div>}
            </div>
          </div>
        )
      }
      
      render(){
        return( 
        <> 
          
              <div>
                  <CFormGroup row>
                    <CCol xs="8" md="6">
                    
                    { this.props.isTeacher === true ? (
                      <CSelect custom name="select" id="select" defaultValue={'DEFAULT'} 
                      onChange={this.selectTeacher}>
                      <option value="DEFAULT" disabled>{this.state.placeholder}</option>

                        {this.state.liste.map((item, index) =>
                        <option key={index} >{item.firstName} {item.lastName.toUpperCase()}</option>)}

                      </CSelect>):(

                      <CSelect custom name="select" id="select" defaultValue={'DEFAULT'} 
                      onChange={this.selectGroup}>
                      <option value="DEFAULT" disabled>{this.state.placeholder}</option>
                      {this.state.liste.map((item, index) =>
                        <option key={index} >{item.name}</option>)}
                      </CSelect>)} 

                    </CCol>
                  </CFormGroup> 
                  {this.state.activeElement && <CCardGroup columns className = "cols-2" >
            
                  <ScheduleComponent 
                  
                  locale='fr-CH'
                  currentView={this.state.displayView} 
                  readonly={true}
                  startHour={'08:00'}
                  endHour={'21:00'}
                  showWeekend={false}
                  allowDragAndDrop={false}
                  workDays={this.workingDays}
                  allowKeyboardInteraction={false}
                  selectedDate={new Date()} 
                  eventSettings={{ 
                    dataSource: this.state.data,
                    template: this.eventTemplate.bind(this),
                    fields: this.fields}}>
                    <Inject services={[Day, Week, WorkWeek, Month]}/>
                    <ViewsDirective>
                      <ViewDirective option='Day' displayName='Jour'/>
                      <ViewDirective option='Week' displayName='Semaine'/>
                      {/* <ViewDirective option='Month' displayName='Mois'  
                      eventTemplate={this.eventTemplateMonth.bind(this)}/>*/}
                    </ViewsDirective>
                  </ScheduleComponent>
                </CCardGroup>}
              </div>
        
        </> 
          );
      }
}

export default Group;
