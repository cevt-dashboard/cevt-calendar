
export function getGroupList (){
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/group-names/all';
    var response = fetch(apiUrl)
        .then(response => response.json())
    return response
}

export function getTeacherList (){
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/teachers/all';
    var response = fetch(apiUrl)
        .then(response => response.json())
    return response
}

export function getGroupDetails (group) {
    var response = fetch(process.env.REACT_APP_API_URL+'/api/v1/calendar/events-by-group/'+group)
      .then(response => response.json());
    return response
}

export function getTeacherDetails (lastName, firstName) {
    var response = fetch(process.env.REACT_APP_API_URL+'/api/v1/calendar/teachers-events/'+lastName+'/'+firstName)
      .then(response => response.json());
    return response
}

export const titleModule = 
new Map([
    ["PROJET DLL - TP", "PRDLL"],
    ["INITIATION A LA RECHERCHE ET PREPARATION AU MEMOIRE - TD", "PREPMEM"],
    ["FOUILLES DE DONNEES - CM", "FODO"],
    ["ARCHITECTURE DES SI - CM", "ARSI"],
    ["ARCHITECTURE ORIENTEE SERVICES - CM", "AOS"],
    ["STRATEGIE ET TI - CM", "GESO"],
    ["DEVELOPPEMENT DE LOGICIEL LIBRE - CM", "DLL"],
    ["DROIT DES CONTRATS - CM", "DROIT"]
  ]);

export function getTitle(value){
    // console.log(titleModule.get(value))
    if (!titleModule.has(value)){
        return value;
    }
    return titleModule.get(value);
}
